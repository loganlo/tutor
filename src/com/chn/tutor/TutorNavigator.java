package com.chn.tutor;

import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.domain.User;
import com.chn.tutor.domain.UserRoles;
import com.chn.tutor.view.TutorViewType;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class TutorNavigator extends Navigator {
	private static final TutorViewType ERROR_VIEW = TutorViewType.VIDEOS;
    private ViewProvider errorViewProvider;
    
    public TutorNavigator(final ComponentContainer container) {
        super(UI.getCurrent(), container);

        initViewChangeListener();
        initViewProviders();
    }
    
    private void initViewChangeListener() {
        addViewChangeListener(new ViewChangeListener() {

            @Override
            public boolean beforeViewChange(final ViewChangeEvent event) {
            	TutorViewType view = TutorViewType.getByViewName(event.getViewName());
            	User user = TutorCommon.getCurrentUser();
            	
            	if(view.getRole().equals(UserRoles.ADMIN)) {
            		if(user != null && user.getRole().equals(UserRoles.ADMIN)) {
            			return true;
            		} else {
            			event.getNavigator().navigateTo(TutorViewType.VIDEOS.getViewName());
            			return false;
            		}
            	}
            	return true;
            }

            @Override
            public void afterViewChange(final ViewChangeEvent event) {
            	//TutorViewType view = TutorViewType.getByViewName(event.getViewName());
            }
        });
    }
    
    private void initViewProviders() {
        // A dedicated view provider is added for each separate view type
        for (final TutorViewType viewType : TutorViewType.values()) {
        	
        	if (viewType.getViewClass() == null)
        		continue;
        	
            ViewProvider viewProvider = new ClassBasedViewProvider(
                    viewType.getViewName(), viewType.getViewClass()) {

                // This field caches an already initialized view instance if the
                // view should be cached (stateful views).
                private View cachedInstance;

                @Override
                public View getView(final String viewName) {
                    View result = null;
                    if (viewType.getViewName().equals(viewName)) {
                        if (viewType.isStateful()) {
                            // Stateful views get lazily instantiated
                            if (cachedInstance == null) {
                                cachedInstance = super.getView(viewType.getViewName());
                            }
                            result = cachedInstance;
                        } else {
                            // Non-stateful views get instantiated every time
                            // they're navigated to
                            result = super.getView(viewType.getViewName());
                        }
                    }
                    return result;
                }
            };

            if (viewType == ERROR_VIEW) {
                errorViewProvider = viewProvider;
            }

            addProvider(viewProvider);
        }

        setErrorProvider(new ViewProvider() {
            @Override
            public String getViewName(final String viewAndParameters) {
                return ERROR_VIEW.getViewName();
            }

            @Override
            public View getView(final String viewName) {
                return errorViewProvider.getView(ERROR_VIEW.getViewName());
            }
        });
    }
}
