package com.chn.tutor.event;

import java.io.Serializable;

import com.chn.tutor.view.TutorViewType;

@SuppressWarnings("serial")
public abstract class TutorEvent implements Serializable {
	
	public static final class UserLoginRequestedEvent implements Serializable {
        private final String userName, password;
        private final String loginType;

        public UserLoginRequestedEvent(final String userName, final String password) {
        	this(userName, password, null);
        }
        
        public UserLoginRequestedEvent(final String userName,
                final String password, final String loginType) {
            this.userName = userName;
            this.password = password;
            this.loginType = loginType;
        }

        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password; 
        }

        public String getLoginType() {
        	return loginType;
        }
    }
	
	public static final class PassDataBetweenViewEvent implements Serializable {
    	private final TutorViewType sourceView;
        private final Object data;

        public PassDataBetweenViewEvent(final TutorViewType sourceView, final Object data) {
            this.sourceView = sourceView;
            this.data = data;
        }

        public TutorViewType getSourceView() {
            return sourceView;
        }
        
        public Object getData() {
        	return data;
        }
    }
	
	public static class VideoUpdatedEvent implements Serializable {
	}
	
	public static class SignInSuccessEvent implements Serializable {
    }
	
	public static class ReloadPageEvent implements Serializable {
    }
	
	public static class CloseOpenWindowsEvent implements Serializable {
    }
	
	public static class UpdateClientEvent implements Serializable {
	}
}
