package com.chn.tutor.event;

import java.io.Serializable;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import com.chn.tutor.TutorUI;

/**
 * A simple wrapper for Guava event bus. Defines static convenience methods for
 * relevant actions.
 */
/* Added Serialization implementation for GAE deployment. */
//Added by Kaniz 17/03/2015
@SuppressWarnings("serial")
public class TutorEventBus implements SubscriberExceptionHandler, Serializable { 

	/**
	 * Actual class to implement serialization for AsyncEventBus.
	 * @author Kaniz
	 *
	 */
	public static final class SerializableEventBus extends EventBus implements Serializable {
		public SerializableEventBus (SubscriberExceptionHandler handler) {
			super(handler);
		}
	};
	
    private final SerializableEventBus eventBus = new SerializableEventBus(this);
	
    public static void post(final Object event) {
        TutorUI.getTutorEventbus().eventBus.post(event);
    }

    public static void register(final Object object) {
    	TutorUI.getTutorEventbus().eventBus.register(object);
    }

    public static void unregister(final Object object) {
    	TutorUI.getTutorEventbus().eventBus.unregister(object);
    }
    
    @Override
    public final void handleException(final Throwable exception,
            final SubscriberExceptionContext context) {
        exception.printStackTrace();
    }
}
