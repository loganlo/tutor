package com.chn.tutor.domain;

import java.io.Serializable;

public enum VideoTypes implements Serializable {
	COURSE("Course"),
	FUNCLIPS("Fun video clips"),
	OTHERTEACHING("Class from others");
	
	
	private final String descp;
	
	private VideoTypes(String descp) {
		this.descp = descp;
	}
	
	@Override
	public String toString() {
		return descp;
	}
}
