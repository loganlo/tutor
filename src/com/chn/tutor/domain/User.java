/**
 * Copyright (c) 2015 Pymlo. All rights reserved.
 * @author Kaniz Lin
 * @description Domain class to store user information from user management.
 * @note User security and data are mostly stored in the user management resource, like Stormpath.  
 */
package com.chn.tutor.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.chn.tutor.TutorUI;
import com.chn.tutor.stringlib.TutorLocaleTypes;
import com.chn.tutor.util.RandomString;

/* Added Serialization implementation for GAE deployment. */
/* Note: Serialization implementation in all persistent classes is mandatory. */
//Added by Kaniz 17/03/2015
@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_USERS")
public final class User extends AbstractEntity implements Serializable {

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
		this.role = UserRoles.VISITOR;
	}

	public User(String username, String password,
			String firstname, String lastname) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.role = UserRoles.VISITOR;
	}
	
    @NotNull
    @Size(max = 100)
    @Column(name="USERNAME", unique=true)
	private String username = null;
    @NotNull
    @Size(max = 100)
    @Column(name="EMAIL", unique=true)
	private String email = null;
    @NotNull
    @Column(name="PASSWORD")
    private String password = null;
    @Transient
    private String plainPassword = null;
    
    @NotNull
    @Size(min = 2, max = 24)
    @Column(name="FIRST_NAME")
    private String firstname = null;
    @NotNull
    @Size(min = 2, max = 24)
    @Column(name="LAST_NAME")
    private String lastname = null;
    @Column(name="TITLE")
    private String title;
    
    @Size(max = 100)
    @Column(name="FACEBOOK_ACCOUNT", unique=true)
    private String facebookAccount = null;
    
    @Size(max = 100)
    @Column(name="GMAIL_ACCOUNT", unique=true)
    private String gmailAccount = null;
    
    @Enumerated(EnumType.STRING)
    @Column(name="LANGUAGE")
    private TutorLocaleTypes language;
    
    @Enumerated(EnumType.STRING)
    @Column(name="USER_ROLE")
    private UserRoles role;
    
    @Column(name="IS_FIRST_LOGIN")
    private boolean firstLogin;
    
    @OneToMany(mappedBy="user", cascade={CascadeType.ALL}, orphanRemoval=true)
    private List<UserLogin> logins;
    /*
    @OneToMany(mappedBy="user", cascade={CascadeType.ALL}, orphanRemoval=true)
    private List<UserPasswordResetToken> passwordResetTokens;
    */
    public User() {
    	language = TutorLocaleTypes.ENGLISH;
    }
    
    /* This method creates a ghost user automatically.
     * Data of the actual user should be forced to update 
     * upon the first login attempt. 
     */
    public static User createNewUser(String username) {
    	User newUser = new User();
    	
    	newUser.username = username;
    	newUser.email = username;
    	newUser.plainPassword = (new RandomString(12)).nextString();
    	newUser.password = TutorUI.getUserManager().encryptPassword(newUser.plainPassword);
    	
    	return newUser;
    }
   
    @PrePersist
    public void onSave() {
    	this.firstLogin = true;
    }
    
    @Override
	@PreUpdate
    public void onUpdate() {
    	this.firstLogin = false;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public TutorLocaleTypes getLanguage() {
		return language;
	}

	public void setLanguage(TutorLocaleTypes language) {
		this.language = language;
	}
	
	public UserRoles getRole() {
		return role;
	}

	public void setRole(UserRoles role) {
		this.role = role;
	}

	public String getEmail() {
		if(email!=null)
			return email;
		else
			return "";
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		if(firstname!=null)
			return firstname;
		else
			return "";
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		if(lastname!=null)
			return lastname;
		else
			return "";
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	public String getGmailAccount() {
		return gmailAccount;
	}

	public void setGmailAccount(String gmailAccount) {
		this.gmailAccount = gmailAccount;
	}

	public List<UserLogin> getLogins() {
		return logins;
	}

	public void setLogins(List<UserLogin> logins) {
		this.logins = logins;
	}

	public String getPlainPassword() {
		return plainPassword;
	}

	public void setPlainPassword(String plainPassword) {
		this.plainPassword = plainPassword;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
/*
	public List<UserPasswordResetToken> getPasswordResetTokens() {
		return passwordResetTokens;
	}

	public void setPasswordResetTokens(List<UserPasswordResetToken> passwordResetTokens) {
		this.passwordResetTokens = passwordResetTokens;
	}
*/
	public String getFacebookAccount() {
		return facebookAccount;
	}

	public void setFacebookAccount(String facebookAccount) {
		this.facebookAccount = facebookAccount;
	}
}
