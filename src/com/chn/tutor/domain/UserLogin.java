/**
 * Copyright (c) 2015 Pymlo. All rights reserved.
 * @author Kaniz Lin
 * @description Domain class to store user login histories.
 */
package com.chn.tutor.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/* Added Serialization implementation for GAE deployment. */
/* Note: Serialization implementation in all persistent classes is mandatory. */
//Added by Kaniz 17/03/2015
@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_USER_LOGINS")
public final class UserLogin implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
	@NotNull
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private User user;
	
	@Column(name = "DATE_CREATED", insertable = true, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated = null;
    
    public UserLogin() {
    }
    
    public UserLogin(User user) {
    	this();
    	this.user = user;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	@PrePersist
	protected void onCreate() {
		dateCreated = new Date();
	}

}
