package com.chn.tutor.domain;

import java.io.Serializable;

public enum UserRoles implements Serializable {
	ADMIN,
	VISITOR;
	
	private UserRoles() {
		
	}
}
