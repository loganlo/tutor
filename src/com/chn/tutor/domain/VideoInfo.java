package com.chn.tutor.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "DATA_VIDEO_INFO")
public final class VideoInfo  extends AbstractEntity implements Serializable {

	@Column(name="SOURCEURL")
	private String sourceUrl;
	
	@Column(name="YOUTUBEID")
	private String youtubeID;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="NUMCLICKS")
	private Integer numClicks;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="TYPE")
	private VideoTypes type;
	
	public VideoInfo() {	
		type = VideoTypes.FUNCLIPS;
		numClicks = 0;
	}
	
	public VideoInfo(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
	
	public String getYoutubeID() {
		return youtubeID;
	}
	public void setYoutubeID(String youtubeID) {
		this.youtubeID = youtubeID;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public VideoTypes getType() {
		return type;
	}
	public void setType(VideoTypes type) {
		this.type = type;
	}
	
	public Integer getNumClicks() {
		return numClicks;
	}
	public void setNumClicks(Integer numClicks) {
		this.numClicks = numClicks;
	}
}
