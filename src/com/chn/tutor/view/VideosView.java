package com.chn.tutor.view;

import java.util.Iterator;
import java.util.List;

import com.chn.tutor.TutorUI;
import com.chn.tutor.component.SignUpWindow;
import com.chn.tutor.component.VideoContainer;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.view.design.VideosDesign;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class VideosView extends VideosDesign implements View {
	/**
	 * 
	 */
	private static final long serialVersionUID = -95695043678243577L;
	private static final String ID = "videosview";
	private static final int pageContent = 12;
	
	private List<VideoInfo> videoList;
	private int page = 1;
	
	public VideosView() {
		//TutorEventBus.register(this);
		
		btnSignup.addStyleName("signup");
		btnSignup.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				SignUpWindow.open();
			}
		});
		btnSignup.setVisible(false);
		
		videoList = TutorUI.getDataManager().getVideoList();

		loadVideos();
	}
	
	public void loadVideos() {
		//videoList.subList: start is inclusive, end is exclusive
		int start = pageContent*(page-1);
		int end = videoList.size() > pageContent*page ? pageContent*page : videoList.size();
		
		lytVideoGrid.removeAllComponents();
		
		if(videoList.size() > 0) {
			List<VideoInfo> showVideos = videoList.subList(start, end);

			for (Iterator<VideoInfo> i = showVideos.iterator(); i.hasNext();) {
				VideoInfo video = i.next();
				lytVideoGrid.addComponent(new VideoContainer(video));
			}
		}
		updatePageSelect();
	}
	
	public void updatePageSelect() {
		int start = page > 5 ? page-4 : 1;
		int endTotal = videoList.size()/pageContent + 1;
		int end = page+4 > endTotal ? endTotal : page+4;
		
		lytPageSelect.removeAllComponents();
		
		if(end == 1) return;
		else {
			for(int i=start; i<=end; i++) {
				final Button btn = new Button(String.valueOf(i));
				btn.setData(i);
				btn.addStyleName(ValoTheme.BUTTON_SMALL);
				if(i == page) {
					btn.addStyleName(ValoTheme.BUTTON_FRIENDLY);
				}
				else {
					btn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
					btn.addClickListener(new ClickListener() {
						@Override
						public void buttonClick(final ClickEvent event) {
							page = (int)btn.getData();
							loadVideos();
						}
					});
				}
				lytPageSelect.addComponent(btn);
			}
			
			if(page < end) {
				Button btn = new Button("Next");
				btn.addStyleName(ValoTheme.BUTTON_SMALL);
				btn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
				btn.addClickListener(new ClickListener() {
					@Override
					public void buttonClick(final ClickEvent event) {
						page++;
						loadVideos();
					}
				});
				lytPageSelect.addComponent(btn);
			}
		}
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
}
