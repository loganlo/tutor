package com.chn.tutor.view;

import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.component.SignInWindow;
import com.chn.tutor.domain.User;
import com.chn.tutor.domain.UserRoles;
import com.chn.tutor.event.TutorEvent.SignInSuccessEvent;
import com.chn.tutor.event.TutorEvent.UpdateClientEvent;
import com.chn.tutor.event.TutorEventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class TutorOverheadMenu extends HorizontalLayout implements Command {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6486295566629541975L;
	
	public static final String ID = "accoverheadmenu";
	
	private MenuBar menu;
	private MenuItem userItem;
	private MenuItem userProfileItem;
	private MenuItem adminItem;
	private MenuItem userLogOutItem;
	private MenuItem localeItem;
	
	public TutorOverheadMenu() {
		setId(ID);
		setSpacing(true);
		Responsive.makeResponsive(this);
		setSizeFull();
		addStyleName("mainmenu");
		
		TutorEventBus.register(this);
		
		Label logo = new Label("CINREW");
		logo.addStyleName(ValoTheme.LABEL_H1);
		logo.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		HorizontalLayout logoWrapper = new HorizontalLayout(logo);
		logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
		logoWrapper.addStyleName(ValoTheme.MENU_TITLE);
		addComponent(logoWrapper);
		
		
		Button btnHomeView = new Button(TutorViewType.HOME.getViewCaption());
		btnHomeView.setId("overheadmenu_btnhome");
		btnHomeView.setIcon(TutorViewType.HOME.getIcon());
		btnHomeView.addStyleName("menubutton");
		btnHomeView.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		btnHomeView.addStyleName(ValoTheme.BUTTON_LARGE);
		btnHomeView.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				Page.getCurrent().open("http://188.166.249.172/", null);
			}
		});
		addComponent(btnHomeView);
		setComponentAlignment(btnHomeView, Alignment.MIDDLE_LEFT);
		
		Button btnVideosView = new Button(TutorViewType.VIDEOS.getViewCaption());
		btnVideosView.setId("overheadmenu_btnvideo");
		btnVideosView.setIcon(TutorViewType.VIDEOS.getIcon());
		btnVideosView.addStyleName("menubutton");
		btnVideosView.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		btnVideosView.addStyleName(ValoTheme.BUTTON_LARGE);
		btnVideosView.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				UI.getCurrent().getNavigator().navigateTo(TutorViewType.VIDEOS.getViewName());
			}
		});
		addComponent(btnVideosView);
		setComponentAlignment(btnVideosView, Alignment.MIDDLE_LEFT);
		
		Button btnAbout = new Button("About");
		btnAbout.addStyleName("menubutton");
		btnAbout.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		btnAbout.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				Page.getCurrent().open("http://188.166.249.172/index.php/about/", null);
			}
		});
		addComponent(btnAbout);
		setComponentAlignment(btnAbout, Alignment.MIDDLE_RIGHT);
		
		Button btnContact = new Button("Contact");
		btnContact.addStyleName("menubutton");
		btnContact.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		btnContact.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				Page.getCurrent().open("http://188.166.249.172/index.php/contact/", null);
			}
		});
		addComponent(btnContact);
		setComponentAlignment(btnContact, Alignment.MIDDLE_LEFT);
		setExpandRatio(btnContact, 1);
		
		menu = new MenuBar();
		menu.setSizeUndefined();
		menu.addStyleName("menuitem");
		menu.addStyleName(ValoTheme.MENUBAR_BORDERLESS);
		addComponent(menu);
		setComponentAlignment(menu, Alignment.MIDDLE_RIGHT);
		
		if(TutorCommon.getCurrentUser() != null && 
				TutorCommon.getCurrentUser().getRole().equals(UserRoles.ADMIN)) {
			User user = TutorCommon.getCurrentUser();
			userItem = menu.addItem(user.getFirstname(), FontAwesome.USER, this);
			userItem.setCommand(null);
			adminItem = userItem.addItem(TutorViewType.ADMIN.getViewCaption(), this);
			
		} else if (TutorCommon.getCurrentUser() != null) {
			//Normal users
		} else {
			userItem = menu.addItem("Sign In", FontAwesome.USER, this);
		}
	}
	
	@Override
	public void menuSelected(MenuItem selectedItem) {
		if (selectedItem.equals(userItem)) {
			SignInWindow.open();
		} 
		else if (selectedItem.equals(adminItem)) {
			UI.getCurrent().getNavigator().navigateTo(
					TutorViewType.ADMIN.getViewName());
		}
		else if (selectedItem.equals(userProfileItem)) {
			//ProfilePreferencesWindow.open(AccCommon.getCurrentUser(), 0);
		}
		else if (selectedItem.equals(userLogOutItem)) {
			//AccEventBus.post(new UserLoggedOutEvent());
		}
	}
	
	@Subscribe
    public void updateUserNme(SignInSuccessEvent event) {
		User user = TutorCommon.getCurrentUser();
		
		userItem.setText(user.getFirstname());
		userItem.setCommand(null);
		
		if(user.getRole().equals(UserRoles.ADMIN))
			adminItem = userItem.addItem(TutorViewType.ADMIN.getViewCaption(), this);
		
		TutorEventBus.post(new UpdateClientEvent());
    }
}
