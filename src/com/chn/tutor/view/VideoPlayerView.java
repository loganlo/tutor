package com.chn.tutor.view;

import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.view.design.VideoPlayerDesign;
import com.chn.tutor.event.TutorEvent.PassDataBetweenViewEvent;
import com.google.common.eventbus.Subscribe;
import com.kbdunn.vaadin.addons.mediaelement.MediaElementPlayer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Responsive;
import com.vaadin.ui.VerticalLayout;

public class VideoPlayerView extends VideoPlayerDesign implements View {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8994749707473845693L;
	private static final String ID = "videoplayerview";
	private static final String YoutubeVideoHeader = "https://youtu.be/";
	
	private VideoInfo videoInfo;
	
	private MediaElementPlayer videoPlayer;
	
	public VideoPlayerView() {
		Responsive.makeResponsive(this);
		
		videoPlayer = new MediaElementPlayer(MediaElementPlayer.Type.VIDEO);
		videoPlayer.setWidth("640px");
		videoPlayer.setHeight("440px");
		lytVideo.addComponent(videoPlayer);
	}
	
	@Subscribe
	public void dataReceivedFromView(PassDataBetweenViewEvent event) {
		if (event != null) {
			this.videoInfo = (VideoInfo)event.getData();
			lblVideoCaption.setValue(videoInfo.getTitle());
			lblDesc.setValue(videoInfo.getDescription());
			videoPlayer.setSource(new ExternalResource(YoutubeVideoHeader+videoInfo.getYoutubeID()));
		}
	}
	
	@Override
	public void detach() {
		super.detach();
		TutorEventBus.unregister(this);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
		TutorEventBus.register(this);
	}	
}
