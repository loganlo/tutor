package com.chn.tutor.view;

import com.chn.tutor.TutorNavigator;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.VerticalLayout;

public class MainView extends VerticalLayout {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 5500418716033165595L;

	public MainView() {
		 addComponent(new TutorOverheadMenu());
		 
		 ComponentContainer ctnContent = new CssLayout();
	        ctnContent.addStyleName("view-content");
	        ctnContent.addStyleName("v-scrollable");
	        ctnContent.setSizeFull();
	        addComponent(ctnContent);
	        setExpandRatio(ctnContent, 1.0f);
	        
	        new TutorNavigator(ctnContent);
	 }
}
