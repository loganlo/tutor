package com.chn.tutor.view;

import java.io.Serializable;

import com.chn.tutor.domain.UserRoles;
import com.vaadin.navigator.View;
import com.vaadin.server.Resource;

public enum TutorViewType implements Serializable {
	HOME("home","Home",null,null,true,true,UserRoles.VISITOR),
	VIDEOS("videos","Videos",VideosView.class,null,true,true,UserRoles.VISITOR),
	VIDEOPLAYER("videoplayer","VideoPlayer",VideoPlayerView.class,null,true,false,UserRoles.VISITOR),
	COURSE("course","Course",null,null,true,false,UserRoles.VISITOR),
	TUTOR("tutor","Tutor",null,null,true,false,UserRoles.VISITOR),
	
	//Admin
	ADMIN("admin","Admin",AdminView.class,null,true,false,UserRoles.ADMIN);
	
    private final String viewName;
    private final String viewCaption;
    private final Class<? extends View> viewClass;
    private final Resource icon;
    private final boolean stateful;
    private final boolean inMenu;
    private final UserRoles role;
    
    private TutorViewType(String viewName, String viewCaption,
    		final Class<? extends View> viewClass, final Resource icon, 
    		final boolean stateful, final boolean inMenu,
    		final UserRoles role) {
    	this.viewName = viewName;
    	this.viewCaption = viewCaption;
    	this.viewClass = viewClass;
    	this.icon = icon;
    	this.stateful = stateful;
    	this.inMenu = inMenu;
    	this.role = role;
    }
    
    public boolean isStateful() {
        return stateful;
    }
    
    public boolean isInMenu() {
        return inMenu;
    }

    public String getViewName() {
        return viewName;
    }
    
    public String getViewCaption() {
    	return viewCaption;
    }

    public Class<? extends View> getViewClass() {
        return viewClass;
    }

    public Resource getIcon() {
        return icon;
    }
    
    public UserRoles getRole() {
        return role;
    }

    public static TutorViewType getByViewName(final String viewName) {
    	TutorViewType result = null;
        for (TutorViewType viewType : values()) {
            if (viewType.getViewName().equals(viewName)) {
                result = viewType;
                break;
            }
        }
        return result;
    }
}
