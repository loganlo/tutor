package com.chn.tutor.view;

import com.chn.tutor.TutorUI;
import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.component.EditVideoWindow;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.event.TutorEvent.VideoUpdatedEvent;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.view.design.AdminDesign;
import com.google.common.eventbus.Subscribe;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class AdminView extends AdminDesign implements View {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2882272465269258455L;
	public static final String ID = "adminview";
	
	private final JPAContainer<VideoInfo> videos;
	
	private VideoInfo selectedVideo;
	
	public AdminView() {
		TutorEventBus.register(this);
		
		videos = TutorUI.getDataManager().getJpaContainer(VideoInfo.class);
		
		btnNew.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				EditVideoWindow.open(new VideoInfo());
			}
		});
		
		btnEdit.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				EditVideoWindow.open(selectedVideo);
			}
		});
		btnEdit.setEnabled(false);
		
		btnDelete.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				try {
					videos.removeItem(selectedVideo.getId());
					videos.commit();
					TutorCommon.displaySuccessNotification("Delete successfully!!");
				} catch (Exception e) {
					TutorCommon.displayErrorNotification("Error. Failed to delete.");
				}
				
		    	if (videos.size() <= 0) {
		    		btnEdit.setEnabled(false);
		    		btnDelete.setEnabled(false);
		    	}
			}
		});
		btnDelete.setEnabled(false);
		
		buildTable();
	}
	
	private void buildTable() {
		tblVideos.setContainerDataSource(videos);
		
		tblVideos.setImmediate(true);
		tblVideos.setSelectable(true);
		tblVideos.setColumnReorderingAllowed(false);
		tblVideos.setSortContainerPropertyId("dateCreated");
		tblVideos.setSortAscending(false);
		
		tblVideos.setVisibleColumns("dateCreated", "title", "type", "youtubeID");
		tblVideos.setColumnHeaders(
        		"Date", "Title", "Type", "YoutubeID");
		
		tblVideos.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(final ValueChangeEvent event) {
            	if (event.getProperty().getValue() != null) {
            		selectedVideo = videos.getItem(event.getProperty().getValue()).getEntity();
            		
            		btnEdit.setEnabled(true);
            		btnDelete.setEnabled(true);
            	}  else {
            		btnEdit.setEnabled(false);
            		btnDelete.setEnabled(false);
            	}
            }
		});
	}
	
	@Subscribe
    public void videoUpdated(final VideoUpdatedEvent event) {
    	videos.refresh();
	}
	
	@Override
	public void detach() {
		super.detach();
		TutorEventBus.unregister(this);
	}
	
	@Override
	public void enter(ViewChangeEvent event) {
	}
}
