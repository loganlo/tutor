package com.chn.tutor.stringlib;

import java.io.Serializable;

public enum TutorLocaleTypes implements Serializable {
	ENGLISH("en", "English", true), 
	THAI("th", "ภาษาไทย", true);

	private final String localeName;
	private final String menuCaption;
	private final boolean inMenu;

	private TutorLocaleTypes(final String localeName, final String menuCaption,
			final boolean inMenu) {
		this.localeName = localeName;
		this.menuCaption = menuCaption;
		this.inMenu = inMenu;
	}

	public String getLocaleName() {
		return localeName;
	}

	public String getCaption() {
		return menuCaption;
	}

	public boolean isInMenu() {
		return inMenu;
	}
	
	@Override
	public String toString() {
		return menuCaption;
	}
	
    public static TutorLocaleTypes getByLocale(final String locale) {
    	TutorLocaleTypes result = null;
        for (TutorLocaleTypes localeType : values()) {
            if (localeType.getLocaleName().equals(locale)) {
                result = localeType;
                break;
            }
        }
        return result;
    }
}
