package com.chn.tutor.data;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.chn.tutor.data.DataStorageException;
import com.chn.tutor.domain.User;
import com.chn.tutor.domain.VideoInfo;

/**
 * Pymlo- Asian Cloud Accounting backend API to manipulate data in abstracted data source.
 */
public interface DataManager {
    /**
     * @param obj
     *            Data for storage or persistence.
     * @throws DataStorageException 
     */
    void save(Object obj) throws DataStorageException;
    
    /**
     * @param obj
     *            Data for merging into existing entities.
     * @return Merged state of data object.
     * @throws DataStorageException 
     */
    <T> T merge(T obj) throws DataStorageException;
    
    /**
     * @param entityClass
     * @param primaryKey
     * @return Found data instance from data storage.
     * @throws DataStorageException 
     */
    <T> T find(Class<T> entityClass, Object primaryKey) throws DataStorageException;
    
    /**
     * @param obj
     *            Data for refresh from data storage
     * @return Success on data refresh.
     * @throws DataStorageException 
     */
    void refresh(Object obj) throws DataStorageException;
    
    /**
     * @param obj
     *            Data for deletion.
     * @return Success on data deletion.
     * @throws DataStorageException 
     */
    void delete(Object obj) throws DataStorageException;

    /**
     * @param objs
     *            Sets of objects for deletion.
     * @param className
     *            Class name of deleted objects.
     * @return Success on data deletion.
     * @throws DataStorageException 
     */
    void delete(Set<Object> objs, String className) throws DataStorageException;
    
    /**
     * @param <T>
     *            the type of entity to be contained in the JPAContainer
     * @param entityClass
     *            the class of the entity
     * @return a fully configured JPAContainer instance
     */
    <T> JPAContainer<T> getJpaContainer (Class<T> entityClass);
    
    /**
     * @param <T>
     *            the type of entity to be contained in the JPAContainer
     * @param entityClass
     *            the class of the entity
     * @param readOnly
     * @param cached
     * @return a fully configured JPAContainer instance
     */
    <T> JPAContainer<T> getJpaContainer (Class<T> entityClass, boolean readOnly, boolean cached);
    
    
    User getUserInfo(String userName, String accountType);
    /**
     * @param username
     * @param token
     * @return A valid password reset token to verify user's access.
     */
    //UserPasswordResetToken getUserPasswordResetToken(String username, String token);

	List<VideoInfo> getVideoList();
    
}
