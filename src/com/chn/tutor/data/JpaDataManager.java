package com.chn.tutor.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.chn.tutor.data.DataManager;
import com.chn.tutor.domain.User;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.domain.VideoTypes;
import com.chn.tutor.util.DateUtil;
import com.chn.tutor.util.JsonUtil;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.server.VaadinRequest;
import com.vaadin.util.CurrentInstance;

/**
 * An implementation for the backend API to extract data from the database.
 * Anatomy: Data provider interface - Data provider implementation -
 * SQLContainer - JDBC bean - Database
 */
// Added by Kaniz 17/03/2015
@SuppressWarnings("serial")
public class JpaDataManager implements DataManager, Serializable {

	// Persistence unit name for data store
	private static final String PERSISTENCE_UNIT = "tutor";

	private static Random rand = new Random();
	private static Date lastDataUpdate;

	private static final Logger log = Logger.getLogger(JpaDataManager.class
			.getName());

	private static EntityManager em;

	/**
	 * Initialize the data for this application.
	 */
	public JpaDataManager() {
		try {
			// Get an entity manager
			em = JPAContainerFactory
					.createEntityManagerForPersistenceUnit(PERSISTENCE_UNIT);
		} catch (Exception e) {
			// TODO: exception handling when data store connection failed.
			e.printStackTrace();
			log.warning("Error occured when connecting to the persistence unit.");
		}
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, -1);
		//if (lastDataUpdate == null || lastDataUpdate.before(cal.getTime())) {
			//refreshStaticData();
			lastDataUpdate = new Date();
		//}
	}

	
	/**
	 * @param obj
	 *            Data for storage or persistence.
	 * @return Success on data storage.
	 */
	@Override
	public void save(Object obj) throws DataStorageException {
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			em.persist(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			e.printStackTrace();
			throw new DataStorageException();
		}
	}
	
    /**
     * Explicitly merge existing entities for those who know what they are doing.
     * 
     * @param obj
     *            Data for merging into existing entities.
     * @return Success on data storage.
     */
	@Override
    public <T> T merge(T obj) throws DataStorageException {
		EntityTransaction tx = em.getTransaction();
		T ret;
		try {
			tx.begin();
			ret = em.merge(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			e.printStackTrace();
			throw new DataStorageException();
		}
		return ret;
    }

    /**
     * @param entityClass
     * @param primaryKey
     * @return Found data instance from data storage.
     * @throws DataStorageException 
     */
    public <T> T find(Class<T> entityClass, Object primaryKey) throws DataStorageException {
		T ret;
		try {
			ret = em.find(entityClass, primaryKey);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataStorageException();
		}
		return ret;
    }
	
	/**
	 * @param obj
	 *            Data for refresh from data storage
	 * @return Success on data refresh.
	 */
	@Override
	public void refresh(Object obj) throws DataStorageException {
		try {
			em.refresh(obj);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataStorageException();
		}
	}

	/**
	 * @param obj
	 *            Data for storage or persistence.
	 * @return Success on data deletion.
	 */
	@Override
	public void delete(Object obj) throws DataStorageException {
		EntityTransaction tx = em.getTransaction();
		try {
			tx.begin();
			if (em.contains(obj))
				em.remove(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx.isActive())
				tx.rollback();
			e.printStackTrace();
			throw new DataStorageException();
		}
	}

	/**
	 * @param objs
	 *            Sets of objects for deletion.
	 * @param className
	 *            Class name of deleted objects.
	 * @return 
	 * @return Success on data deletion.
	 */
	@Override
	public void delete(Set<Object> objs, String className) throws DataStorageException {
		// TODO
		return;
	}

	/**
	 * @param <T>
	 *            the type of entity to be contained in the JPAContainer
	 * @param entityClass
	 *            the class of the entity
	 * @return a fully configured JPAContainer instance
	 */
	@Override
	public <T> JPAContainer<T> getJpaContainer(Class<T> entityClass) {
		return getJpaContainer(entityClass, false, true);
	}
	
    /**
     * @param <T>
     *            the type of entity to be contained in the JPAContainer
     * @param entityClass
     *            the class of the entity
     * @param readOnly
     * @param cached
     * @return a fully configured JPAContainer instance
     */
	@Override
    public <T> JPAContainer<T> getJpaContainer (Class<T> entityClass, boolean readOnly, boolean cached) {
		JPAContainer<T> ret;
		
		if (readOnly && !cached) {
			ret = JPAContainerFactory.makeNonCachedReadOnly(entityClass, em);
		} else if (readOnly) {
			ret = JPAContainerFactory.makeReadOnly(entityClass, em);
		} else if (!cached) {
			ret = JPAContainerFactory.makeNonCached(entityClass, em);
		} else {
			ret = JPAContainerFactory.make(entityClass, em);
		}
		
		return ret;
	}

	@Override
	public User getUserInfo(String userName, String accountType) {
		String accountTypeColumn = "username";
		if ("GOOGLE".equals(accountType))
			accountTypeColumn = "googleAccount";
		
		try{
			return (User) em
					.createQuery(
							"SELECT u FROM User u WHERE LOWER(u." + accountTypeColumn + ") = :username")
					.setParameter("username", userName.toLowerCase()).setMaxResults(1)
					.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<VideoInfo> getVideoList() {
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<VideoInfo> cq = cb.createQuery(VideoInfo.class);
			Root<VideoInfo> video = cq.from(VideoInfo.class);
			
			cq.select(video);
			cq.where(cb.equal(video.<VideoTypes>get("type"),VideoTypes.FUNCLIPS));
			
			cq.orderBy(cb.desc(video.<Date>get("dateCreated")));
			TypedQuery<VideoInfo> q = em.createQuery(cq);
			List<VideoInfo> videoList = q.getResultList();
			
			return videoList;
		} catch (Exception e) {
			return null;
		}
	}
	
	/*
	@Override
	public UserPasswordResetToken getUserPasswordResetToken(String username, String token) {
		try{
			//Add time restriction
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			return (UserPasswordResetToken) em
					.createQuery(
							"SELECT t FROM UserPasswordResetToken t, User u"
							+ " WHERE t.user = u AND LOWER(u.username) = :username AND t.id = :token "
							+ "AND t.used = false AND t.dateCreated >= :dateBefore ")
					.setParameter("username", username.toLowerCase())
					.setParameter("token", token)
					.setParameter("dateBefore", cal.getTime())
					.setMaxResults(1)
					.getResultList().get(0);
		} catch (Exception e) {
			return null;
		}
	}
*/
}
