package com.chn.tutor.data;

import java.io.Serializable;

public class DataStorageException extends Exception implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 6042872970199455313L;

	//Parameterless Constructor
    public DataStorageException() {}

    //Constructor that accepts a message
    public DataStorageException(String message)
    {
       super(message);
    }
    
}
