package com.chn.tutor.util.shiro;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.StringUtils;

public class OAuthRealm implements Realm {

	@Override
	public AuthenticationInfo getAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        OAuthToken authToken = (OAuthToken) token;

        PrincipalCollection principals;

        try {
            principals = createPrincipals(authToken);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new AuthenticationException("Unable to obtain authenticated account properties.", e);
        }

        AuthenticationInfo info = new SimpleAuthenticationInfo(principals, null);
        return info;
	}
	
    protected PrincipalCollection createPrincipals(OAuthToken token) {

        LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();

        props.put("id", token.getId());
        nullSafePut(props, "username", token.getUsername());
        nullSafePut(props, "firstName", token.getFirstName());
        nullSafePut(props, "lastName", token.getLastName());

        Collection<Object> principals = new ArrayList<Object>(2);
        principals.add(token.getId());
        principals.add(props);

        return new SimplePrincipalCollection(principals, getName());
    }
    
    private void nullSafePut(Map<String, String> props, String propName, String value) {
        value = StringUtils.clean(value);
        if (value != null) {
            props.put(propName, value);
        }
    }
	
	@Override
	public boolean supports(AuthenticationToken token) {
	    return token != null && token instanceof OAuthToken;
	}

	@Override
	public String getName() {
		return OAuthRealm.class.getName();
	}
}