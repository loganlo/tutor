package com.chn.tutor.util.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.UnavailableSecurityManagerException;
import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;

import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;

public class VaadinSecurityUtils extends SecurityUtils {

	/**
	 * The attribute name used in the {@link VaadinSession} to store the
	 * {@link Subject}.
	 */
	private static final String SUBJECT_ATTRIBUTE = VaadinSecurityUtils.class.getName() + ".subject";
	private static final String INI_RESOURCE_PATH = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath()
			+ "/WEB-INF/shiro.ini";
	
	/**
	 * The security manager for the application.
	 */
	private static SecurityManager securityManager = managerFromIni();

	private static SecurityManager managerFromIni() {
		IniSecurityManagerFactory iniSecurityManagerFactory = new IniSecurityManagerFactory(INI_RESOURCE_PATH);
		DefaultSecurityManager manager = (DefaultSecurityManager) iniSecurityManagerFactory.getInstance();
		// manager.setRememberMeManager(new VaadinCookieRememberMeManager());
		return manager;
	}

	/**
	 * Returns the subject for the application and thread which represents the
	 * current user. The subject is always available; however it may represent
	 * an anonymous user.
	 * 
	 * @return the subject for the current application and thread
	 * @see SecurityUtils#getSubject()
	 */
	public static Subject getSubject() {
		VaadinSession session = VaadinSession.getCurrent();
		if (session == null) {
			throw new IllegalStateException("Unable to locate VaadinSession to store Shiro Subject.");
		}

		Subject subject = (Subject) session.getAttribute(SUBJECT_ATTRIBUTE);
		if (subject == null) {
			subject = new Subject.Builder(getSecurityManager()).buildSubject();
			session.setAttribute(SUBJECT_ATTRIBUTE, subject);
		}

		return subject;
	}

	/**
	 * Sets the security manager for the application. To support push, normally
	 * a {@link DefaultSecurityManager} is used rather than a web specific one
	 * because the normal HTTP request/response cycle isn't used.
	 * 
	 * @param securityManager
	 *            the security manager to set
	 */
	public static void setSecurityManager(SecurityManager securityManager) {
		VaadinSecurityUtils.securityManager = securityManager;
	}

	public static SecurityManager getSecurityManager() throws UnavailableSecurityManagerException {

		SecurityManager securityManager = VaadinSecurityUtils.securityManager;
		if (securityManager == null) {
			String msg = "No SecurityManager accessible to the calling code.";
			throw new UnavailableSecurityManagerException(msg);
		}

		return securityManager;
	}
}

/*
 * 
 * 
 * import static org.apache.shiro.web.mgt.CookieRememberMeManager.
 * DEFAULT_REMEMBER_ME_COOKIE_NAME; import static
 * org.apache.shiro.web.servlet.Cookie.DELETED_COOKIE_VALUE; import static
 * org.apache.shiro.web.servlet.Cookie.ONE_YEAR; import static
 * org.apache.shiro.web.servlet.Cookie.ROOT_PATH;
 * 
 * import javax.servlet.http.Cookie;
 * 
 * import org.apache.shiro.codec.Base64; import
 * org.apache.shiro.mgt.AbstractRememberMeManager; import
 * org.apache.shiro.subject.Subject; import
 * org.apache.shiro.subject.SubjectContext; import
 * org.apache.shiro.web.mgt.CookieRememberMeManager; import org.slf4j.Logger;
 * import org.slf4j.LoggerFactory;
 * 
 * import com.vaadin.server.VaadinService;
 * 
 * public class VaadinCookieRememberMeManager extends AbstractRememberMeManager
 * {
 * 
 * private static transient final Logger log =
 * LoggerFactory.getLogger(CookieRememberMeManager.class); private Cookie
 * cookie;
 * 
 * public VaadinCookieRememberMeManager() { Cookie cookie = new
 * Cookie(DEFAULT_REMEMBER_ME_COOKIE_NAME, ""); cookie.setMaxAge(ONE_YEAR);
 * cookie.setPath(ROOT_PATH); cookie.setHttpOnly(true); this.cookie = cookie; }
 * 
 * public Cookie getCookie() { return cookie; }
 * 
 * protected void rememberSerializedIdentity(Subject subject, byte[] serialized)
 * {
 * 
 * //base 64 encode it and store as a cookie: String base64 =
 * Base64.encodeToString(serialized);
 * 
 * Cookie cookie = copyTemplate(); cookie.setValue(base64);
 * VaadinService.getCurrentResponse().addCookie(cookie); }
 * 
 * private Cookie copyTemplate() { Cookie template = getCookie(); Cookie cookie
 * = new Cookie(template.getName(), ""); cookie.setMaxAge(template.getMaxAge());
 * cookie.setPath(template.getPath());
 * cookie.setHttpOnly(template.isHttpOnly()); return cookie; }
 * 
 * protected byte[] getRememberedSerializedIdentity(SubjectContext
 * subjectContext) { Cookie cookie = getCookieByName(getCookie().getName());
 * String base64 = cookie != null ? cookie.getValue() : null;
 * 
 * // Browsers do not always remove cookies immediately (SHIRO-183) // ignore
 * cookies that are scheduled for removal if
 * (DELETED_COOKIE_VALUE.equals(base64)) { return null; }
 * 
 * if (base64 != null) { base64 = ensurePadding(base64); if
 * (log.isTraceEnabled()) { log.trace("Acquired Base64 encoded identity [" +
 * base64 + "]"); } byte[] decoded = Base64.decode(base64); if
 * (log.isTraceEnabled()) { log.trace("Base64 decoded byte array length: " +
 * (decoded != null ? decoded.length : 0) + " bytes."); } return decoded; } else
 * { return null; } }
 * 
 * private String ensurePadding(String base64) { int length = base64.length();
 * if (length % 4 != 0) { StringBuilder sb = new StringBuilder(base64); for (int
 * i = 0; i < length % 4; ++i) { sb.append('='); } base64 = sb.toString(); }
 * return base64; }
 * 
 * protected void forgetIdentity(Subject subject) { Cookie cookie =
 * getCookieByName(getCookie().getName()); if (cookie != null) {
 * cookie.setPath(getCookie().getPath()); cookie.setValue(DELETED_COOKIE_VALUE);
 * cookie.setMaxAge(0); VaadinService.getCurrentResponse().addCookie(cookie); }
 * }
 * 
 * public void forgetIdentity(SubjectContext subjectContext) {
 * forgetIdentity(subjectContext.getSubject()); }
 * 
 * private Cookie getCookieByName(String name) { Cookie[] cookies =
 * VaadinService.getCurrentRequest().getCookies(); for (Cookie cookie : cookies)
 * { if (name.equals(cookie.getName())) { return cookie; } }
 * 
 * return null; }
 * 
 * }
 * 
 * 
 * 
 * 
 * 
 * import javax.enterprise.inject.Specializes;
 * 
 * import com.vaadin.cdi.access.JaasAccessControl;
 * 
 * @Specializes public class ShiroAccessControl extends JaasAccessControl {
 * 
 * @Override public boolean isUserSignedIn() { return
 * VaadinSecurityUtils.getSubject().isAuthenticated(); }
 * 
 * @Override public boolean isUserInRole(String role) { return
 * VaadinSecurityUtils.getSubject().hasRole(role); }
 * 
 * @Override public String getPrincipalName() { Object principal =
 * VaadinSecurityUtils.getSubject().getPrincipal(); return principal != null ?
 * principal.toString() : null; } }
 */