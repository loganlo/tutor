package com.chn.tutor.util.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/* Overriden authentication token class with username only to bypass internal check when logging in via oauth. */
public class OAuthToken implements AuthenticationToken  {
	
	private String id;
	private String username;
	private String firstName;
	private String lastName;

	public OAuthToken(String id, String username, String firstName, String lastName) {
		this.id = id;
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
	public String getPrincipal() {
		return username;
	}

	@Override
	public String getCredentials() {
		return username;
	}

	public String getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}
}