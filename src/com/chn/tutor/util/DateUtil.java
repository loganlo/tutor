/**
 * Copyright (c) 2015 Pymlo. All rights reserved.
 * @author Kaniz Lin
 * @description Utility class for Date-related and temporal operations.
 */
package com.chn.tutor.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class DateUtil implements Serializable {
    
    /**
     * @param time
     *            Date instance designated to extract day.
     * @return Parsed day of the given time.
     */
    public static Date getDay(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        return cal.getTime();
    }

    /**
     * @param time
     *            Date instance designated to extract month.
     * @return Parsed month of the given time.
     */
    public static Date getMonth(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, 0);
        return cal.getTime();
    }
    
    /**
     * @param time
     *            Date instance designated to extract year.
     * @return Parsed year of the given time.
     */
    public static Date getYear(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, 0);
        cal.set(Calendar.MONTH, 0);
        return cal.getTime();
    }
    
    public static Date getFirstDayOfWeek(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);

        int th = cal.get(Calendar.DAY_OF_WEEK);
        cal.add(Calendar.DAY_OF_MONTH, -th+1);
        return cal.getTime();
    }
    
    public static Date getFirstDayOfYear(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.MONTH, 0);
        return cal.getTime();
    }
    /**
     * @param year
     *            Year of the specified date.
     * @param month
     *            Month of the specified date.
     * @param dayOfMonth
     *            Day of month of the specified date.
     * @return Generated data instance based on the date given.
     */
    public static Date getSpecificDate(int year, int month, int dayOfMonth) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        cal.set(Calendar.MONTH, month-1);
        cal.set(Calendar.YEAR, year);
        return cal.getTime();
    }
    
    public static Date getDateAfterNDays(Date time, int NDays) {
    	 Calendar cal = Calendar.getInstance();
         cal.setTime(time);
         cal.set(Calendar.MILLISECOND, 0);
         cal.set(Calendar.SECOND, 0);
         cal.set(Calendar.MINUTE, 0);
         cal.set(Calendar.HOUR_OF_DAY, 0);
         cal.add(Calendar.DAY_OF_MONTH, NDays);
         return cal.getTime();
    }
}
