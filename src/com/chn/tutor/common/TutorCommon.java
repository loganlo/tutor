package com.chn.tutor.common;

import java.util.Locale;

import com.chn.tutor.domain.User;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class TutorCommon {

	public static final String APP_URL = "https://myaccounting.pymlo.com";
	public static final String LAND_URL = "";
	public static final String LAND_ABOUT_PATH = "";
	
	public static User getCurrentUser() {
		return (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
	}
	
	public static Locale getCurrentLocale() {
		User user = getCurrentUser();
		if (user != null) {
			if (user.getLanguage() != null) {
				return new Locale(user.getLanguage().getLocaleName());
			}
		}
		return new Locale("en");
	}
	
	public static void displaySuccessNotification(String msg) {
		Notification notification = new Notification("Sweet!", msg);
		notification.setDelayMsec(2000);
		notification.setStyleName("bar success");
		notification.setPosition(Position.TOP_CENTER);
		notification.show(Page.getCurrent());
	}

	public static void displayErrorNotification(String msg) {
		Notification notification = new Notification("Heads up!", msg);
		notification.setDelayMsec(4000);
		notification.setStyleName("bar failure");
		notification.setPosition(Position.TOP_CENTER);
		notification.show(Page.getCurrent());
	}
}
