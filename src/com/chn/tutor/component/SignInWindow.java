package com.chn.tutor.component;

import com.chn.tutor.component.design.SignInLayout;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.CloseOpenWindowsEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Responsive;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class SignInWindow extends Window {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4462211282216939532L;
	public static final String ID = "signinwindow";
	
	public SignInWindow() {
		//addStyleName("profile-window");
    	//addStyleName("moviedetailswindow");
        setId(ID);
        Responsive.makeResponsive(this);
        
        center();
        setModal(true);
        addCloseShortcut(KeyCode.ESCAPE, null);
        setResizable(false);
        setWidth(320, Unit.PIXELS);
        
        setContent(new SignInLayout());
	}
	
	public static void open() {
        TutorEventBus.post(new CloseOpenWindowsEvent());
        Window w = new SignInWindow();
        UI.getCurrent().addWindow(w);
        w.focus();
    }
	
	@Override
	public void close() {
		super.close();
	}
}
