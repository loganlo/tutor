package com.chn.tutor.component;

import com.chn.tutor.component.design.SignUpLayout;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.CloseOpenWindowsEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Responsive;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class SignUpWindow extends Window {
	/**
	 * 
	 */
	private static final long serialVersionUID = 957390485699986873L;
	public static final String ID = "signupwindow";
	
	public SignUpWindow() {
		//addStyleName("profile-window");
    	//addStyleName("moviedetailswindow");
        setId(ID);
        Responsive.makeResponsive(this);
        
        center();
        setModal(true);
        addCloseShortcut(KeyCode.ESCAPE, null);
        setResizable(false);
        setWidth(320, Unit.PIXELS);
        
        setContent(new SignUpLayout());
	}
	
	public static void open() {
        TutorEventBus.post(new CloseOpenWindowsEvent());
        Window w = new SignUpWindow();
        UI.getCurrent().addWindow(w);
        w.focus();
    }
	
	@Override
	public void close() {
		super.close();
	}
}
