package com.chn.tutor.component.design;

import com.chn.tutor.TutorUI;
import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.component.VideoContainer;
import com.chn.tutor.data.DataStorageException;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.domain.VideoTypes;
import com.chn.tutor.event.TutorEvent.VideoUpdatedEvent;
import com.chn.tutor.event.TutorEventBus;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class EditVideoLayout extends EditVideoDesign {

	
	private final BeanFieldGroup<VideoInfo> fieldGroup;
	private final VideoInfo video;
	
	public EditVideoLayout(final VideoInfo v) {
		video = v;
		fieldGroup = new BeanFieldGroup<VideoInfo>(VideoInfo.class);
		fieldGroup.bind(fldTitle, "title");
		fieldGroup.bind(fldDescp, "description");
		fieldGroup.bind(fldYoutubeID, "youtubeID");
		fieldGroup.bind(fldType, "type");
		fieldGroup.setItemDataSource(video);
		
		fldType.addItems(VideoTypes.values());
		
		if(video.getId()==null) {
			lytPreview.setVisible(false);
			lblCaption.setValue("Add Video");
			btnDelete.setVisible(false);
		}
		else {
			VideoContainer lytVideo = new VideoContainer(video);
			lytPreview.addComponent(lytVideo);
			lytPreview.setComponentAlignment(lytVideo, Alignment.MIDDLE_CENTER);
			
			lblCaption.setValue("Edit Video");
			btnDelete.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(final ClickEvent event) {
					try {
						TutorUI.getDataManager().delete(video);
						TutorEventBus.post(new VideoUpdatedEvent());
					} catch (DataStorageException e) {
						TutorCommon.displayErrorNotification("Failed");
						e.printStackTrace();
					}
					close();
				}
			});
		}
			
		btnSave.setClickShortcut(KeyCode.ENTER);
		btnSave.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				
				try {
					fieldGroup.commit();
					
					if(video.getId() == null) {
						TutorUI.getDataManager().save(video);
					} else {
						TutorUI.getDataManager().merge(video);
					}
					
					TutorCommon.displaySuccessNotification("Saved successfully");
		            
		            TutorEventBus.post(new VideoUpdatedEvent());
		            close();
		        } catch (CommitException e) {
		        	TutorCommon.displayErrorNotification("Failed");
		        } catch (DataStorageException e) {
		        	TutorCommon.displayErrorNotification("Failed");
					e.printStackTrace();
				}
			}
		});
		btnCancel.setClickShortcut(KeyCode.ESCAPE);
		btnCancel.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				close();
			}
		});
	}
	
	public void close() {
		if (getParent() instanceof Window)
    		((Window)this.getParent()).close();
	}
}
