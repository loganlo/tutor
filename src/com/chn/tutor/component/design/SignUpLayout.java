package com.chn.tutor.component.design;

import com.chn.tutor.TutorUI;
import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.data.DataStorageException;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.UserLoginRequestedEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class SignUpLayout extends SignUpDesign {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3034313153075678095L;
	
	public SignUpLayout() {
		
		
		btnSignUp.setClickShortcut(KeyCode.ENTER);
		btnSignUp.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				if (fldPasswd.getValue() != null && fldPasswd.getValue().equals(fldConfPasswd.getValue())) {
					try {
						if (TutorUI.getDataManager().getUserInfo(fldEmail.getValue(), null) == null) {
							TutorUI.getUserManager().signUp(
									fldEmail.getValue(), fldPasswd.getValue(),fldFirstName.getValue(), fldLastName.getValue());
							TutorEventBus.post(new UserLoginRequestedEvent(fldEmail.getValue(), fldPasswd.getValue()));
							close();
						} else {
							TutorCommon.displayErrorNotification("This email has already registered.");
						}
					} catch (DataStorageException e) {
						TutorCommon.displayErrorNotification(
								"We are having mysterious trouble creating your data. We will fix it soon!");
						e.printStackTrace();
					} catch (Exception e) {
						TutorCommon.displayErrorNotification("Unexpected error. Please contact support.");
						e.printStackTrace();
					}
				}
			}
		});
		btnCancel.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				close();
			}
		});
	}
	
	public void close() {
		if (getParent() instanceof Window)
    		((Window)this.getParent()).close();
	}
}
