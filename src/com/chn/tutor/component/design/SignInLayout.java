package com.chn.tutor.component.design;

import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.UserLoginRequestedEvent;
import com.chn.tutor.event.TutorEvent.SignInSuccessEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class SignInLayout extends SignInDesign {
	
	public SignInLayout() {
		
		btnSignIn.setClickShortcut(KeyCode.ENTER);
		btnSignIn.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				TutorEventBus.post(new UserLoginRequestedEvent(fldEmail.getValue(), fldPasswd.getValue()));
				close();
			}
		});
		btnCancel.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				close();
			}
		});
	}
	
	public void close() {
		if (getParent() instanceof Window)
    		((Window)this.getParent()).close();
	}
}
