package com.chn.tutor.component;

import com.chn.tutor.TutorUI;
import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.data.DataStorageException;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.PassDataBetweenViewEvent;
import com.chn.tutor.view.TutorViewType;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public class VideoContainer extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3050916966997701552L;
	private static final String YoutubeImgHeader = "http://img.youtube.com/vi/";
	private static final String YoutubeImgTail = "/hqdefault.jpg";
	
	private final VideoInfo videoInfo;
	
	public VideoContainer(final VideoInfo videoInfo) {
		this.videoInfo = videoInfo;
		setWidth("140px");
		setSpacing(true);
		setHeightUndefined();
		
		Button btnVideo = new Button();
		btnVideo.addStyleName(ValoTheme.BUTTON_ICON_ONLY);
		btnVideo.addStyleName(ValoTheme.BUTTON_BORDERLESS);
		btnVideo.addStyleName("thumbnail");
		btnVideo.setIcon(new ExternalResource(YoutubeImgHeader+videoInfo.getYoutubeID()+YoutubeImgTail));
		btnVideo.setWidth("196px");
		btnVideo.setHeight("132px");
		btnVideo.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(final ClickEvent event) {
				
				videoInfo.setNumClicks(videoInfo.getNumClicks()+1);
				
				try {
					TutorUI.getDataManager().merge(videoInfo);
				} catch (DataStorageException e) {
                    TutorCommon.displayErrorNotification("Oops! Sorry we cannot access video.");
					e.printStackTrace();
				}
				
				UI.getCurrent().getNavigator().navigateTo(
						TutorViewType.VIDEOPLAYER.getViewName());
            	
            	TutorEventBus.post(new PassDataBetweenViewEvent(TutorViewType.VIDEOS, videoInfo));
			}
		});
		addComponent(btnVideo);
		
    	Label des = new Label(videoInfo.getTitle());
    	des.setWidth("90%");
    	addComponent(des);
    	setComponentAlignment(des, Alignment.TOP_CENTER);
    	
    	HorizontalLayout lytNumClicks = new HorizontalLayout();
    	lytNumClicks.setSpacing(true);
    	Label lblIcon = new Label();
    	lblIcon.setIcon(FontAwesome.HEADPHONES);
    	Label lblNumClicks = new Label(videoInfo.getNumClicks().toString());
    	lytNumClicks.addComponents(lblIcon,lblNumClicks);
    	addComponent(lytNumClicks);
	}
	
	public VideoInfo getVideoInfo() {
		return videoInfo;
	}
}
