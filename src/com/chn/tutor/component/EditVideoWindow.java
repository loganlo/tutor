package com.chn.tutor.component;

import com.chn.tutor.component.design.EditVideoLayout;
import com.chn.tutor.domain.VideoInfo;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.event.TutorEvent.CloseOpenWindowsEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Responsive;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

public class EditVideoWindow extends Window {
	/**
	 * 
	 */
	private static final long serialVersionUID = 627462585265494180L;
	public static final String ID = "editvideowindow";
	
	public EditVideoWindow(VideoInfo video) {
		//addStyleName("profile-window");
    	//addStyleName("moviedetailswindow");
        setId(ID);
        Responsive.makeResponsive(this);
        
        center();
        setModal(true);
        addCloseShortcut(KeyCode.ESCAPE, null);
        setResizable(false);
        setWidth(600, Unit.PIXELS);
        
        setContent(new EditVideoLayout(video));
	}
	
	public static void open(VideoInfo video) {
        TutorEventBus.post(new CloseOpenWindowsEvent());
        Window w = new EditVideoWindow(video);
        UI.getCurrent().addWindow(w);
        w.focus();
    }
	
	@Override
	public void close() {
		super.close();
	}
}
