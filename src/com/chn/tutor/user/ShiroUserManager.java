package com.chn.tutor.user;

import java.io.Serializable;
import java.util.logging.Logger;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.DefaultPasswordService;

import com.chn.tutor.TutorUI;
import com.chn.tutor.domain.User;
import com.chn.tutor.domain.UserLogin;
import com.chn.tutor.domain.UserRoles;
import com.chn.tutor.user.UserManager;
import com.chn.tutor.util.shiro.OAuthToken;
import com.chn.tutor.util.shiro.VaadinSecurityUtils;
import com.vaadin.server.VaadinSession;

/**
 * An implementation for the backend API to extract data from the database.
 * Anatomy: Data provider interface - Data provider implementation -
 * SQLContainer - JDBC bean - Database
 */
// Added by Kaniz 17/03/2015
@SuppressWarnings("serial")
public class ShiroUserManager implements UserManager, Serializable {

	private static final Logger log = Logger.getLogger(ShiroUserManager.class.getName());

	/**
	 * Initialize the data for this application.
	 */
	public ShiroUserManager() {
	}

	@Override
	public User authenticate(String userName, String password, String loginType) {

		Subject currentUser = VaadinSecurityUtils.getSubject();

		if (loginType == null) {
			if (!currentUser.isAuthenticated()) {
				UsernamePasswordToken token = new UsernamePasswordToken(userName.toLowerCase(), password);
				try {
					currentUser.login(token);
				} catch (UnknownAccountException uae) {
					log.warning("Unsuccessful login UnknownAccountException by user " + userName);
				} catch (IncorrectCredentialsException ice) {
					log.warning("Unsuccessful login IncorrectCredentialsException by user " + userName);
				} catch (LockedAccountException lae) {
					log.warning("Unsuccessful login LockedAccountException by user " + userName);
				} catch (ExcessiveAttemptsException eae) {
					log.warning("Unsuccessful login ExcessiveAttemptsException by user " + userName);
				} catch (AuthenticationException ae) {
					log.warning("Unsuccessful login AuthenticationException by user " + userName);
				} catch (Exception e) {
					log.severe("Unexpected error upon authentication: " + e.getMessage());
					e.printStackTrace();
				}

				if (currentUser.isAuthenticated()) {
					try {
						/* Retrieve remaining user information */
						User user = TutorUI.getDataManager().getUserInfo(userName, null);

						/* Record login time */
						TutorUI.getDataManager().save(new UserLogin(user));

						return user;
					} catch (Exception e) {
						log.severe("Unexpected error with data storage after authentication: " + e.getMessage());
						e.printStackTrace();
						return null;
					}
				} else {
					log.warning("Unsuccessful login without locatable user " + userName);
					return null;
				}
			}
		} else if (loginType.startsWith("OAUTH")) {

			try {
				/* Retrieve remaining user information */
				User user;
				if ((user = TutorUI.getDataManager().getUserInfo(userName, loginType)) == null)
					user = TutorUI.getDataManager().getUserInfo(userName, null);

				OAuthToken token = (OAuthToken) VaadinSession.getCurrent().getAttribute(OAuthToken.class.getName());
				currentUser.login(token);
				
				/* Record login time */
				TutorUI.getDataManager().save(new UserLogin(user));

				return user;
			} catch (Exception e) {
				log.severe("Unexpected error with data storage after authentication: " + e.getMessage());
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	@Override
	public boolean logOut() {
		Subject currentUser = VaadinSecurityUtils.getSubject();
		if (currentUser.isAuthenticated()) {
			currentUser.logout();
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void signUp(String userName, String password) throws Exception {
		User user = new User(userName, encryptPassword(password));
		user.setEmail(userName);

		/* Make the new account persistent. */
		TutorUI.getDataManager().save(user);
	}

	@Override
	public void signUp(String userName, String password, String firstname, String lastname) throws Exception {
		User user = new User(userName, encryptPassword(password));
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setEmail(userName);

		//TODO
		user.setRole(UserRoles.ADMIN);
		
		/* Make the new account persistent. */
		TutorUI.getDataManager().save(user);
	}
	
	public void signUp(String userName, String password, 
    		String firstname, String lastname, String oauthType) throws Exception {
		User user = new User(userName, encryptPassword(password));
		user.setFirstname(firstname);
		user.setLastname(lastname);
		user.setEmail(userName);
		
		if (oauthType != null) {
			if (oauthType.contains("GOOGLE"))
				user.setGmailAccount(userName);
			else if (oauthType.contains("FACEBOOK"))
				user.setFacebookAccount(userName);		}

		/* Make the new account persistent. */
		TutorUI.getDataManager().save(user);
	}

	@Override
	public String encryptPassword(String password) {
		DefaultPasswordService passwordService = new DefaultPasswordService();
		String encryptedValue = passwordService.encryptPassword(password);
		return encryptedValue;
	}
}
