package com.chn.tutor.user;

import com.chn.tutor.domain.User;

/**
 * Pymlo- Asian Cloud Accounting backend API for user management.
 */
//Added by Kaniz 23/04/2015
public interface UserManager {
    /**
     * @param userName
     * @param password
     * @param rememberMe
     * @param loginType
     * @return Authenticate user.
     */
    User authenticate(String userName, String password, String loginType);
    
    /**
     * @return Success on user log out.
     */
    boolean logOut();
    
    /**
     * @param userName
     * @param password
     * @return Register a new user to the directory.
     * @throws Exception 
     */
    void signUp(String userName, String password) throws Exception;
    
    /**
     * @param userName
     * @param password
     * @param firstname
     * @param lastname
     * @return Register a new user to the directory.
     * @throws Exception 
     */
    void signUp(String userName, String password, 
    		String firstname, String lastname) throws Exception;
    
    /**
     * @param userName
     * @param password
     * @param firstname
     * @param lastname
     * @param oauthType
     * @return Register a new user to the directory via oauth.
     * @throws Exception 
     */
    void signUp(String userName, String password, 
    		String firstname, String lastname,
    		String oauthType) throws Exception;
    
    /**
     * @param password
     * @return Encrypted password based on designated encryption methodology. 
     */
    String encryptPassword(String password);
    
}
