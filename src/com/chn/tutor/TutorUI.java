package com.chn.tutor;

import java.util.ArrayList;
import com.chn.tutor.common.TutorCommon;
import com.chn.tutor.data.DataManager;
import com.chn.tutor.data.JpaDataManager;
import com.chn.tutor.domain.User;
import com.chn.tutor.event.TutorEventBus;
import com.chn.tutor.user.UserManager;
import com.chn.tutor.user.ShiroUserManager;
import com.chn.tutor.TutorUI;
import com.chn.tutor.view.MainView;
import com.google.common.eventbus.Subscribe;
import com.chn.tutor.event.TutorEvent.CloseOpenWindowsEvent;
import com.chn.tutor.event.TutorEvent.ReloadPageEvent;
import com.chn.tutor.event.TutorEvent.UserLoginRequestedEvent;
import com.chn.tutor.event.TutorEvent.SignInSuccessEvent;
import com.chn.tutor.event.TutorEvent.UpdateClientEvent;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.util.FileTypeResolver;

@Title("CINREW Chinese Learing Platform")
@SuppressWarnings("serial")
@Theme("tutor")
@Widgetset("com.chn.tutor.widgetset.TutorWidgetset")
@Push
public class TutorUI extends UI {
	
	 private final DataManager dataManager = new JpaDataManager();
	 private final UserManager userManager = new ShiroUserManager();
	 private final TutorEventBus tutorEventBus = new TutorEventBus();
	 
	 private final JPAContainer<User> users = dataManager.getJpaContainer(User.class);
/*
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = TutorUI.class)
	public static class Servlet extends VaadinServlet {
	}
*/
	@Override
	protected void init(VaadinRequest request) {
		TutorEventBus.register(this);
        Responsive.makeResponsive(this);
        
        //For MediaElementPlayer, configure MIME type mappings
        FileTypeResolver.addExtension("ogg", "audio/ogg");
        FileTypeResolver.addExtension("ogv", "video/ogg");
        FileTypeResolver.addExtension("mp4", "video/mp4");
        FileTypeResolver.addExtension("webm", "video/webm");
        FileTypeResolver.addExtension("wmv", "video/x-ms-wmv");
        FileTypeResolver.addExtension("wma", "audio/x-ms-wma");
        FileTypeResolver.addExtension("flv", "video/x-flv");
        FileTypeResolver.addExtension("avi", "video/x-msvideo");
        
        
		updateContent();
	}
	
	
	
	private void updateContent() {
        setContent(new MainView());
        getNavigator().navigateTo(getNavigator().getState());
    }
	
	
	@Subscribe
    public void userLoginRequested(final UserLoginRequestedEvent event) {
		User user = null;
    	if(event.getLoginType() == null) {
    		/* Direct login from website or login view. */
	        user = getUserManager().authenticate(event.getUserName(),
	                event.getPassword(), event.getLoginType());
		    
	        if (user == null) {
				TutorCommon.displayErrorNotification("Incorrect email or password.");
	        } 
        }
    	
    	 if(user != null) {   
 	       VaadinSession.getCurrent().setAttribute(User.class.getName(), user);
 	       TutorEventBus.post(new SignInSuccessEvent());
 	    }
	}
	
	@Subscribe
    public void reloadPage(ReloadPageEvent event) {
        updateContent();
    }
	
	@Subscribe
    public void closeOpenWindows(final CloseOpenWindowsEvent event) {
        for(Window w : new ArrayList<Window>(getWindows())) {
        	w.close();
        }
    }
	
	public static DataManager getDataManager() {
        return ((TutorUI) getCurrent()).dataManager;
    }

	public static UserManager getUserManager() {
        return ((TutorUI) getCurrent()).userManager;
    }
	
	public static TutorEventBus getTutorEventbus() {
        return ((TutorUI) getCurrent()).tutorEventBus;
    }
	
	//Server push
	@Subscribe
    public void updateClient(UpdateClientEvent event) {
    	new UpdateClientThread().start();
    }
	
	public class UpdateClientThread extends Thread {
        @Override
        public void run() {

            // Init done, update the UI after doing locking
            access(new Runnable() {
                @Override
                public void run() {
                    // Here the UI is locked and can be updated
                }
            });
        }
    }
}