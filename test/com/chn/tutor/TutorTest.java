import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;

import com.vaadin.testbench.ScreenshotOnFailureRule;
import com.vaadin.testbench.TestBenchTestCase;
import com.vaadin.testbench.elements.ButtonElement;
import com.vaadin.testbench.elements.LabelElement;


/**
 * This class contains JUnit tests, which are run using Vaadin TestBench 4.
 *
 * To run this, first get an evaluation license from
 * https://vaadin.com/addon/vaadin-testbench and follow the instructions at
 * https://vaadin.com/directory/help/installing-cval-license to install it.
 *
 * Once the license is installed, you can run this class as a JUnit test.
 */
public class TutorTest extends TestBenchTestCase {
    @Rule
    public ScreenshotOnFailureRule screenshotOnFailureRule =
            new ScreenshotOnFailureRule(this, true);

    @Before
    public void setUp() throws Exception {
        // To use Chrome, first install chromedriver.exe from
        // http://chromedriver.storage.googleapis.com/index.html
        // on your system path (e.g. C:\Windows\System32\)
        //File file = new File("/Users/loganlo/Downloads/chromedriver");
    	File file = new File("/home/loganlo/chromedriver");
    	System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
        setDriver(new ChromeDriver()); // Chrome

        // To use Internet Explorer, first install iedriverserver.exe from
        // http://selenium-release.storage.googleapis.com/index.html?path=2.43/
        // on your system path (e.g. C:\Windows\System32\)
        //   setDriver(new InternetExplorerDriver()); // IE

        // To test headlessly (without a browser), first install phantomjs.exe
        // from http://phantomjs.org/download.html on your system path
        // (e.g. C:\Windows\System32\)
        //   setDriver(new PhantomJSDriver()); // PhantomJS headless browser
    }

    /**
     * Opens the URL where the application is deployed.
     */
    private void openTestUrl() {
        getDriver().get("localhost:8080/tutor3");//"http://188.166.249.172:8080/");
    }

    @Test
    public void testClickButton() throws Exception {
        openTestUrl();
        assertFalse($(ButtonElement.class).id("overheadmenu_btnvideo").isDisplayed());
        assertFalse($(LabelElement.class).exists());
    }
}